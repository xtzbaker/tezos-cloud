provider "scaleway" {
#   access_key      = SET ENVIRONMENT VARIABLE TO SCW_ACCESS_KEY
#   secret_key      = SET ENVIRONMENT VARIABLE foe SCW_SECRET_KEY
  zone            = "fr-par-1"
  region          = "fr-par"
}

resource "scaleway_instance_ip" "public_ip" {}

resource "scaleway_instance_security_group" "tezos_public_node" {
  inbound_default_policy  = "drop"
  outbound_default_policy = "accept"

  inbound_rule {
    action = "accept"
    port   = "22"
    ip     = var.my_public_ip
  }

  inbound_rule {
    action = "accept"
    port   = "80"
  }

  inbound_rule {
    action = "accept"
    port   = "443"
  }
}

resource "scaleway_instance_server" "tezos-node" {
  type  = "DEV1-M"
  image = "ubuntu-focal"

  tags = [ "tezos-node", "tezos-public" ]

  ip_id = scaleway_instance_ip.public_ip.id

  security_group_id = scaleway_instance_security_group.tezos_public_node.id

  cloud_init = file("${path.module}/cloud-init.yml")
}